function oldPassword() {
    var a = document.getElementById("old-pass");
    if (a.type === "password") {
        a.type = "text";
    } else {
        a.type = "password";
    }
}

function newPassword() {
    var b = document.getElementById("new-pass");
    if (b.type === "password") {
        b.type = "text";
    } else {
        b.type = "password";
    }
}

function repeatPassword() {
    var c = document.getElementById("repeat-pass");
    if (c.type === "password") {
        c.type = "text";
    } else {
        c.type = "password";
    }
}

window.onload = function () {
    document.getElementById('submit').onclick = function () {
        var pass1 = document.getElementById('new-pass').value;
        var pass2 = document.getElementById('repeat-pass').value;
        if (pass1 && pass2) {
            if (pass1 == pass2)
                alert('Password changed!');
            else
                alert("The two passwords don't match")
        }
    }
}